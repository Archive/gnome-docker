/*
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <linux/input.h>
#include <X11/XF86keysym.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "gnome-docker.h"
#include "gd-dbus.h"
#include "gd-notify.h"
#include "gd-input.h"

#define KEY_UNDOCK 207

static GdkFilterReturn gdk_filter(GdkXEvent *xevent, GdkEvent *event, gpointer data)
{
	XEvent *ev = (XEvent*)xevent;
	printf("filter: %d\n", ev->xkey.keycode);
	if (ev->type != KeyPress)
		return GDK_FILTER_CONTINUE;
	if (ev->xkey.keycode == 207) {
		if (!gd_dbus_send_undock_request())
			gd_notify_warn(_("Undock"), _("Failed to undock device"));
		return GDK_FILTER_CONTINUE;
	}

	/* event not handled */
	return GDK_FILTER_CONTINUE;
}

void gd_input_init(void)
{
	Display *dpy;
	Window root;
	GdkScreen *screen = gdk_screen_get_default ();
	GdkWindow *win = gdk_screen_get_root_window (screen);
	GdkDisplay *display = gdk_display_get_default ();

	dpy = GDK_DISPLAY_XDISPLAY(display);
	root = GDK_WINDOW_XID(win);

/*	XGrabKey(dpy, XKeysymToKeycode(dpy, XStringToKeysym("a")), AnyModifier,*/
	XGrabKey(dpy, KEY_UNDOCK, AnyModifier,
		 root, True, GrabModeAsync, GrabModeAsync);

	gdk_window_add_filter(win, gdk_filter, NULL);
}
