/*
 * gdocker-dbus.h
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GNOME_DOCKER_DBUS_H
#define GNOME_DOCKER_DBUS_H

#include <dbus/dbus.h>

DBusHandlerResult dbus_filter_function(DBusConnection *connection,
				       DBusMessage *message,
				       void *user_data);

gboolean gd_dbus_send_undock_request(void);

gboolean gd_dbus_have_dock(char *udi);

gboolean gd_dbus_get_dock(char **udi);

gboolean gd_dbus_init(void);

#endif /* GNOME_DOCKER_DBUS_H */
