#include "gd-xrandr.h"

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <glib.h>

#include "gnome-docker.h"

int gd_xrandr_adjust_displays(void)
{
	pid_t pid = fork();

	if (pid == -1) {
		gd_error("forking");
		return -1;
	} else if (pid == 0) {
		/* child */
		char *argv[3];

		argv[0] = g_strdup("xrandr");
		argv[1] = g_strdup("--auto");
		argv[2] = NULL;

		execvp(argv[0], argv);
		gd_error("%s: %s\n", argv[0], strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* parent: wait for child */
	int status;
	int ret_code = -1;

	while (wait(&status) != pid)
		; /* empty */

	if (WIFSIGNALED(status)) {
		ret_code = WTERMSIG(status);
	} else if (WIFEXITED(status)) {
		ret_code = WEXITSTATUS(status);
	}

	gd_info("xrandr returned with code: %d", ret_code);

	return 0;
}
