/*
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gtk/gtk.h>

#include "gnome-docker.h"
#include "gd-tray.h"
#include "gd-dbus.h"
#include "gd-input.h"
#include "gd-xrandr.h"

int main(int argc, char *argv[])
{
	char *udi;

	gtk_init(&argc, &argv);

	bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

	if (!gd_dbus_init()) {
		gd_error("Could not init DBus connection, exiting...");
		exit(EXIT_FAILURE);
	}

	if (!gd_dbus_get_dock(&udi)) {
		gd_info("No docking station detected, exiting...\n");
		exit(EXIT_SUCCESS);
	}

	if (!gd_tray_icon_init(udi)) {
		gd_error("Could not init tray icon, exiting...");
		exit(EXIT_FAILURE);
	}
	free(udi);

	gd_input_init();

	gtk_main();

	return 0;
}

