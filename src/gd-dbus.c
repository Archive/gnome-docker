/*
 * gdocker-dbus.c
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <string.h>
#include <liblazy.h>

#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "gnome-docker.h"
#include "gd-dbus.h"
#include "gd-notify.h"
#include "gd-tray.h"
#include "gd-xrandr.h"

#define DBUS_HAL_INTERFACE		"org.freedesktop.Hal"
#define DBUS_HAL_DOCK_INTERFACE		"org.freedesktop.Hal.Device.DockStation"
#define HAL_UDI_COMPUTER		"/org/freedesktop/Hal/devices/platform_dock_0"

gboolean gd_dbus_get_dock(char **udi)
{
	char **devices;

	if (liblazy_hal_find_device_by_capability("dock_station", &devices)) {
		gd_error("Error looking for dock device");
		return FALSE;
	}

	if (devices && *devices) {
		*udi = g_strdup(*devices);
		liblazy_free_strlist(devices);
		return TRUE;
	}

	return FALSE;
}

gboolean gd_dbus_send_undock_request(void)
{
	DBusMessage *reply = NULL;
	int error;
	int ret;
	int docked;
	char *device;

	if (!gd_dbus_get_dock(&device)) {
		gd_warn("Could not get dock devices");
		return FALSE;
	}

	if (liblazy_hal_get_property_bool(device, "info.docked", &docked)) {
		gd_error("Cannot get docking status");
		return FALSE;
	}
	if (docked == 0)
		return TRUE;
	error = liblazy_dbus_system_send_method_call(DBUS_HAL_INTERFACE,
						     HAL_UDI_COMPUTER,
						     DBUS_HAL_DOCK_INTERFACE,
						     "Undock",
						     &reply,
						     DBUS_TYPE_INVALID);

	if (error) {
		gd_warn("Received error code %d", error);
		return FALSE;
	}

	error = liblazy_dbus_message_get_basic_arg(reply, DBUS_TYPE_INT32, &ret, 0);

	if (error) {
		gd_warn("Could not get reply. Error code %d", error);
		goto Error;
	}

	if (ret != 0) {
		gd_warn("Failed to undock. Reply was %d", ret);
		goto Error;
	}

	return TRUE;

Error:
	if (reply)
		dbus_message_unref(reply);
	return FALSE;

}


DBusHandlerResult dbus_filter_function(DBusConnection *connection, DBusMessage *message,
				       void *user_data)
{
	const char *member = dbus_message_get_member(message);
	const char *path = dbus_message_get_path(message);

	if (!path)
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	if (!member) {
		gd_warn("Empty message member for path %s", path);
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}

	if (!g_strrstr(path, "platform_dock"))
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

	if (dbus_message_is_signal(message, DBUS_INTERFACE_LOCAL,
				   "Disconnected")) {
		gd_warn("DBus daemon disconnected. Trying to reconnect...");
		dbus_connection_unref(connection);
		g_timeout_add(5000, (GSourceFunc)gd_dbus_init, NULL);

	} else if (!strcmp(member, "PropertyModified")) {
		DBusMessageIter iter;
		DBusMessageIter iter_array;
		int i, num_modifications;

		dbus_message_iter_init (message, &iter);
		dbus_message_iter_get_basic (&iter, &num_modifications);
		dbus_message_iter_next (&iter);

		dbus_message_iter_recurse (&iter, &iter_array);

		for (i = 0; i < num_modifications; i++) {
			dbus_bool_t removed, added;
			char *key;
			DBusMessageIter iter_struct;

			dbus_message_iter_recurse (&iter_array, &iter_struct);

			dbus_message_iter_get_basic (&iter_struct, &key);
			dbus_message_iter_next (&iter_struct);
			dbus_message_iter_get_basic (&iter_struct, &removed);
			dbus_message_iter_next (&iter_struct);
			dbus_message_iter_get_basic (&iter_struct, &added);

			dbus_message_iter_next (&iter_array);

			if (!strcmp(key, "info.docked")) {
				int docked;

				if (liblazy_hal_get_property_bool(path, "info.docked", &docked)) {
					gd_error("Could not get dock status from DBusMessage");
					return DBUS_HANDLER_RESULT_HANDLED;
				}

				if (docked) {
					gd_notify_info(_("Docked"),
						       _("Your computer has been docked"));
					gd_tray_icon_update_status(TRUE);
				} else {
					gd_notify_info(_("Undocked"),
						       _("Your computer has been undocked"));
					gd_tray_icon_update_status(FALSE);
				}
				gd_xrandr_adjust_displays();
			}

			gd_info("Received: key: %s , removed: %d , added: %d", 
				     key, removed,added);
		}

	} else {
		gd_warn("Received unknown DBus message");
	}

	return DBUS_HANDLER_RESULT_HANDLED;
}

gboolean gd_dbus_init(void)
{
	DBusConnection	*dbus_connection;
	DBusError	dbus_error;

	dbus_error_init(&dbus_error);

	dbus_connection = dbus_bus_get(DBUS_BUS_SYSTEM, &dbus_error);
	if (dbus_error_is_set(&dbus_error)) {
		gd_error("Cannot get D-Bus connection: %s", dbus_error.message);
		return FALSE;
	}

	dbus_connection_setup_with_g_main(dbus_connection, NULL);

	dbus_connection_add_filter(dbus_connection, dbus_filter_function,
				   NULL, NULL);

	dbus_bus_add_match(dbus_connection, 
			   "type='signal',"
			   "interface=org.freedesktop.Hal.Device,"
			   "member=PropertyModified", NULL);

	dbus_connection_set_exit_on_disconnect(dbus_connection, 0);

	return TRUE;
}
