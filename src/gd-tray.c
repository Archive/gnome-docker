/*
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <string.h>
#include <liblazy.h>

#include <gtk/gtk.h>

#include "gnome-docker.h"
#include "gd-dbus.h"
#include "gd-notify.h"
#include "gd-tray.h"

static GtkStatusIcon *icon;

void gd_tray_icon_set_visible(gboolean visible)
{
	gtk_status_icon_set_visible(icon, visible);
}

static void gd_tray_icon_show_about_cb (GtkMenuItem *item, gpointer data)
{
	const char *authors[] = {
		"Holger Macht <hmacht@suse.de>",
		NULL};
	const char *license[] = {
		N_("Licensed under the GNU General Public License Version 2"),
		N_("GNOME Docker is free software; you can redistribute it and/or\n"
		   "modify it under the terms of the GNU General Public License\n"
		   "as published by the Free Software Foundation; either version 2\n"
		   "of the License, or (at your option) any later version."),
		N_("GNOME Docker is distributed in the hope that it will be useful,\n"
		   "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
		   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
		   "GNU General Public License for more details."),
		N_("You should have received a copy of the GNU General Public License\n"
		   "along with this program; if not, write to the Free Software\n"
		   "Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA\n"
		   "02110-1301, USA.")
	};
	const char  *translators = _("translator-credits");
	char	    *license_trans;

	/* Translators comment: put your own name here to appear in the about dialog. */
	if (!strcmp (translators, "translator-credits")) {
		translators = NULL;
	}

	license_trans = g_strconcat (_(license[0]), "\n\n", _(license[1]), "\n\n",
				     _(license[2]), "\n\n", _(license[3]), "\n",  NULL);

	gtk_show_about_dialog (NULL,
			       "name", "gdocker",
			       "version", "0.1",
			       "copyright", "Copyright 2008 Holger Macht",
			       "license", license_trans,
			       "comments", "GNOME dock station applet",
			       "authors", authors,
			       "documenters", NULL,
			       "artists", NULL,
			       "translator-credits", translators,
			       "logo-icon-name", NULL,
			       NULL);
	g_free (license_trans);
}

static void tray_icon_undock_cb(GtkStatusIcon *status_icon)
{
	if (!gd_dbus_send_undock_request())
		gd_notify_warn(_("Undock"), _("Failed to undock device"));
}

static void tray_icon_popup_menu_cb (GtkStatusIcon *icon)
{
	GtkMenu *menu = (GtkMenu*) gtk_menu_new ();
	GtkWidget *item;
	GtkWidget *image;

	item = gtk_image_menu_item_new_with_mnemonic ("Undock");
	image = gtk_image_new_from_icon_name(GTK_STOCK_PREFERENCES,
					     GTK_ICON_SIZE_MENU);
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (tray_icon_undock_cb), icon);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	/* About */
	item = gtk_image_menu_item_new_with_mnemonic (_("_About"));
	image = gtk_image_new_from_icon_name (GTK_STOCK_ABOUT, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	g_signal_connect(G_OBJECT (item), "activate",
			 G_CALLBACK(gd_tray_icon_show_about_cb), icon);
	gtk_menu_shell_append(GTK_MENU_SHELL (menu), item);

	/* show the menu */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
			gtk_status_icon_position_menu, icon,
			1, gtk_get_current_event_time());
}

void gd_tray_icon_update_status(gboolean docked)
{
	if (docked) {
		gtk_status_icon_set_tooltip(icon, _("Status: System docked"));
		gd_tray_icon_set_visible(TRUE);
	} else {
		gd_tray_icon_set_visible(FALSE);
	}
}

gboolean gd_tray_icon_init(const char *udi)
{
    int docked;
    gchar *icon_path;

    icon = gtk_status_icon_new();

    icon_path = g_strdup_printf("%s/%s/gnome-docker.png", DATADIR, PACKAGE_NAME);
    gtk_status_icon_set_from_file(icon, icon_path);
    g_free(icon_path);

    if (liblazy_hal_get_property_bool(udi, "info.docked", &docked)) {
	    gd_error("Cannot get docking status");
	    return FALSE;
    }

    g_signal_connect_object(G_OBJECT (icon),
			    "popup_menu",
			    G_CALLBACK (tray_icon_popup_menu_cb),
			    icon, 0);
    g_signal_connect_object(G_OBJECT (icon),
			    "activate",
			    G_CALLBACK (tray_icon_popup_menu_cb),
			    icon, 0);

    if (docked)
	    gd_tray_icon_update_status(TRUE);
    else
	    gd_tray_icon_update_status(FALSE);

    return TRUE;
}
