/*
 * gdocker-xrandr.h
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GNOME_DOCKER_XRANDR_H
#define GNOME_DOCKER_XRANDR_H

int gd_xrandr_adjust_displays(void);

#endif /* GNOME_DOCKER_XRANDR_H */
