/*
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GNOME_DOCKER_NOTIFY_H
#define GNOME_DOCKER_NOTIFY_H

#include <libnotify/notify.h>

void gd_notify_error(const char *summary, const char *body);

void gd_notify_warn(const char *summary, const char *body);

void gd_notify_info(const char *summary, const char *body);

#endif /* GNOME_DOCKER_NOTIFY_H */
