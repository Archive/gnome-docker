/*
 * gdocker-tray.h
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GNOME_DOCKER_TRAY_H
#define GNOME_DOCKER_TRAY_H

gboolean gd_tray_icon_init(const char *udi);

void gd_tray_icon_set_visible(gboolean visible);

void gd_tray_icon_update_status(gboolean docked);

#endif /* GNOME_DOCKER_TRAY_H */
