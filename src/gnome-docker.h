/*
 * gdocker.h
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GDOCKER_H
#define GDOCKER_H

#include <libintl.h>

#include "gd-i18n.h"

#define gd_info(string, args...) \
do{\
	fprintf(stdout, "INFO (%s:%d) "string"\n", __FUNCTION__, __LINE__, ## args); \
}while(0);

#define gd_warn(string, args...) \
do{\
	fprintf(stdout, "WARNING (%s:%d) "string"\n", __FUNCTION__, __LINE__, ## args); \
}while(0);

#define gd_error(string, args...) \
do{\
	fprintf(stderr, "ERROR (%s:%d) "string"\n", __FUNCTION__, __LINE__, ## args); \
}while(0);

#endif /* GDOCKER_H */
