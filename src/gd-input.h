/*
 * gdocker-input.h
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifndef GNOME_DOCKER_INPUT_H
#define GNOME_DOCKER_INPUT_H

void gd_input_init(void);

#endif /* GNOME_DOCKER_INPUT_H */
