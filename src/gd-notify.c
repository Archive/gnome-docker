/*
 * gdocker-notify.c
 *
 * Copyright (C) 2008 Holger Macht <hmacht@suse.de>
 *
 * This file is released under the GPLv2.
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif /* HAVE_CONFIG_H */

#include <gtk/gtk.h>
#include <libnotify/notify.h>
#ifndef NOTIFY_CHECK_VERSION
#define NOTIFY_CHECK_VERSION(x,y,z) 0
#endif


#include "gd-notify.h"

static void gd_notify(const char *summary,
		      const char *body,
		      NotifyUrgency urgency)
{
	const char *notify_icon;
	NotifyNotification *notify;

	g_return_if_fail(summary != NULL);
	g_return_if_fail(body != NULL);

	if (!notify_is_initted())
		notify_init("gnome-docker");

	notify_icon = GTK_STOCK_NETWORK;
	notify = notify_notification_new (summary, body, notify_icon
#if NOTIFY_CHECK_VERSION (0, 7, 0)
			);
#else
			, NULL);
#endif
	notify_notification_set_urgency(notify, urgency);
	notify_notification_show(notify, NULL);
}

void gd_notify_error(const char *summary, const char *body)
{
	gd_notify(summary, body, NOTIFY_URGENCY_CRITICAL);
}


void gd_notify_warn(const char *summary, const char *body)
{
	gd_notify(summary, body, NOTIFY_URGENCY_NORMAL);
}

void gd_notify_info(const char *summary, const char *body)
{
	gd_notify(summary, body, NOTIFY_URGENCY_LOW);
}
